<?php
namespace PaxfulBundle\Form;

use PaxfulBundle\Entity\Balance;
use PaxfulBundle\Entity\Offer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('type', ChoiceType::class, [
//                'choices' => [
//                    'Buy BTC' => Offer::TYPE_BUY,
//                    'Sell BTC' => Offer::TYPE_SELL,
//                ],
//            ])
            ->add('currency', ChoiceType::class, [
                'choices' => [
                    Balance::CURRENCY_EUR => Balance::CURRENCY_EUR,
                    Balance::CURRENCY_USD => Balance::CURRENCY_USD,
                    Balance::CURRENCY_GPB => Balance::CURRENCY_GPB,
                    Balance::CURRENCY_NGN => Balance::CURRENCY_NGN,
                ],
            ])
            ->add('minAmount', NumberType::class, [
                'scale' => 8
            ])
            ->add('maxAmount', NumberType::class, [
                'scale' => 8
            ])
            ->add('margin', PercentType::class)
            ->add('paymentMethod', ChoiceType::class, [
                'choices' => [
                    'Amazon gift card' => Offer::PAYMENT_METHOD_AMAZON_GIFT_CARD,
                    'Walmart gift card' => Offer::PAYMENT_METHOD_WALMART_GIFT_CARD,
                    'Paypal' => Offer::PAYMENT_METHOD_PAYPAL,
                    'Skrill' => Offer::PAYMENT_METHOD_SKRILL,
                ],
            ])
            ->add('save', SubmitType::class);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PaxfulBundle\Entity\Offer',
        ));
    }
}