<?php
namespace PaxfulBundle\Event;

use PaxfulBundle\Entity\Balance;
use PaxfulBundle\Service\CurrencyService;
use Symfony\Component\EventDispatcher\Event;

class UserEventListener
{
    private const BONUS_BTC = 5;

    private $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    /**
     * @param UserRegisteredEvent|Event $event
     * @return void
     */
    public function onUserRegistered(Event $event): void
    {
        $user = $event->getUser();
        if (!$user->getHasBonus()) {
            $user->setHasBonus(true);
            $this->currencyService->addMoneyToUser($user, self::BONUS_BTC, Balance::CURRENCY_BTC);
        }
    }
}