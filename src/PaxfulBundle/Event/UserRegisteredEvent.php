<?php
namespace PaxfulBundle\Event;

use FOS\UserBundle\Model\UserInterface;
use PaxfulBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;
use FOS\UserBundle\Model\User as BaseUser;

class UserRegisteredEvent extends Event
{
    /** @var User */
    protected $user;


    /**
     * UserRegisteredEvent constructor.
     * @param User|BaseUser|UserInterface $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }


    /**
     * @return null|User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }


    /**
     * @param User $user
     * @return UserRegisteredEvent
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}