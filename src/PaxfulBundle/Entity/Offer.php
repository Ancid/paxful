<?php
namespace PaxfulBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;


/**
 * Offer
 *
 * @ORM\Table(name="offers")
 * @ORM\Entity(repositoryClass="PaxfulBundle\Repository\OfferRepository")
 */
class Offer
{
    const PAYMENT_METHOD_AMAZON_GIFT_CARD = 'Amazon gift card';
    const PAYMENT_METHOD_WALMART_GIFT_CARD = 'Walmart gift card';
    const PAYMENT_METHOD_PAYPAL = 'Paypal';
    const PAYMENT_METHOD_SKRILL = 'Skrill';

    const TYPE_BUY = 'buy';
    const TYPE_SELL = 'sell';

    const STATUS_ACTIVE = 'active';


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="PaxfulBundle\Entity\User", inversedBy="offers")
     * @JoinColumn(name="owner", referencedColumnName="id")
     */
    protected $owner;

    /**
     * @ORM\Column(type="string")
     */
    protected $currency;

    /**
     * @ORM\Column(type="float")
     */
    protected $minAmount;

    /**
     * @ORM\Column(type="float")
     */
    protected $maxAmount;

    /**
     * @ORM\Column(type="float")
     */
    protected $margin;

    /**
     * @ORM\Column(type="string")
     */
    protected $paymentMethod;

    /**
     * @OneToMany(targetEntity="PaxfulBundle\Entity\Trade", mappedBy="offer")
     */
    protected $trades;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = true;

//    /**
//     * @ORM\Column(type="string")
//     */
//    protected $type;




    /**
     * @return array
     */
    public static function getPaymentMethods(): array
    {
        return [
            self::PAYMENT_METHOD_AMAZON_GIFT_CARD,
            self::PAYMENT_METHOD_WALMART_GIFT_CARD,
            self::PAYMENT_METHOD_PAYPAL,
            self::PAYMENT_METHOD_SKRILL,
        ];
    }


//    /**
//     * @return array
//     */
//    public static function getOfferTypes(): array
//    {
//        return [
//            self::TYPE_BUY,
//            self::TYPE_SELL,
//        ];
//    }


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set owner
     * @param User $owner
     * @return Offer
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }


    /**
     * Get owner
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }


    /**
     * @param float $minAmount
     * @return $this
     */
    public function setMinAmount(float $minAmount)
    {
        $this->minAmount = $minAmount;

        return $this;
    }


    /**
     * @return float
     */
    public function getMinAmount()
    {
        return $this->minAmount;
    }


    /**
     * @param float $maxAmount
     * @return $this
     */
    public function setMaxAmount(float $maxAmount)
    {
        $this->maxAmount = $maxAmount;

        return $this;
    }


    /**
     * @return float
     */
    public function getMaxAmount()
    {
        return $this->maxAmount;
    }


    /**
     * @param float $margin
     * @return $this
     */
    public function setMargin(float $margin)
    {
        $this->margin = $margin;

        return $this;
    }


    /**
     * @return float
     */
    public function getMargin()
    {
        return $this->margin;
    }


    /**
     * @param string $paymentMethod
     * @return $this
     */
    public function setPaymentMethod(string $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }


    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trades = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trade
     *
     * @param \PaxfulBundle\Entity\Trade $trade
     *
     * @return Offer
     */
    public function addTrade(\PaxfulBundle\Entity\Trade $trade)
    {
        $this->trades[] = $trade;

        return $this;
    }

    /**
     * Remove trade
     *
     * @param \PaxfulBundle\Entity\Trade $trade
     */
    public function removeTrade(\PaxfulBundle\Entity\Trade $trade)
    {
        $this->trades->removeElement($trade);
    }

    /**
     * Get trades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrades()
    {
        return $this->trades;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Offer
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }


    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Offer
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }


    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
