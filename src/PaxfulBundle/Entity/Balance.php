<?php

namespace PaxfulBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * Balance
 *
 * @ORM\Table(name="balance")
 * @ORM\Entity(repositoryClass="PaxfulBundle\Repository\BalanceRepository")
 */
class Balance
{
    const CURRENCY_BTC = 'BTC';
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_GPB = 'GPB';
    const CURRENCY_NGN = 'NGN';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ManyToOne(targetEntity="PaxfulBundle\Entity\User", inversedBy="balances")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255)
     */
    protected $currency;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    protected $amount = 0.0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return Balance
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Balance
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Balance
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }


    /**
     * @return array
     */
    public static function getCurrencies(): array
    {
        return [
            self::CURRENCY_BTC,
            self::CURRENCY_USD,
            self::CURRENCY_EUR,
            self::CURRENCY_GPB,
            self::CURRENCY_NGN,
        ];
    }

}
