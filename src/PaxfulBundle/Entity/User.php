<?php
namespace PaxfulBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="PaxfulBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @OneToMany(targetEntity="Offer", mappedBy="owner")
     */
    protected $offers;

    /**
     * @OneToMany(targetEntity="Balance", mappedBy="user", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    protected $balances;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $hasBonus = false;

    /**
     * @OneToMany(targetEntity="PaxfulBundle\Entity\Trade", mappedBy="buyer", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    protected $purchases;

    /**
     * @OneToMany(targetEntity="PaxfulBundle\Entity\Trade", mappedBy="seller", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    protected $sales;


    public function __construct() {
        parent::__construct();
        $this->offers = new ArrayCollection();
        $this->balances = new ArrayCollection();
    }


    /**
     * Set offer
     *
     * @param Offer $offer
     *
     * @return User
     */
    public function addOffer(Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }


    /**
     * Get offers
     *
     * @return ArrayCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Remove offer
     *
     * @param \PaxfulBundle\Entity\Offer $offer
     */
    public function removeOffer(\PaxfulBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add balance
     *
     * @param \PaxfulBundle\Entity\Balance $balance
     *
     * @return User
     */
    public function addBalance(\PaxfulBundle\Entity\Balance $balance)
    {
        $this->balances[] = $balance;

        return $this;
    }

    /**
     * Remove balance
     *
     * @param \PaxfulBundle\Entity\Balance $balance
     */
    public function removeBalance(\PaxfulBundle\Entity\Balance $balance)
    {
        $this->balances->removeElement($balance);
    }

    /**
     * Get balances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBalances()
    {
        return $this->balances;
    }


    /**
     * Set hasBonus
     *
     * @param boolean $hasBonus
     *
     * @return User
     */
    public function setHasBonus($hasBonus)
    {
        $this->hasBonus = $hasBonus;

        return $this;
    }

    /**
     * Get hasBonus
     *
     * @return boolean
     */
    public function getHasBonus()
    {
        return $this->hasBonus;
    }


    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }



    /**
     * {@inheritdoc}
     */
    public function setUsername($username)
    {
        $this->username = $this->email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $this->emailCanonical;

        return $this;
    }

    /**
     * Add purchase
     *
     * @param \PaxfulBundle\Entity\Trade $purchase
     *
     * @return User
     */
    public function addPurchase(\PaxfulBundle\Entity\Trade $purchase)
    {
        $this->purchases[] = $purchase;

        return $this;
    }

    /**
     * Remove purchase
     *
     * @param \PaxfulBundle\Entity\Trade $purchase
     */
    public function removePurchase(\PaxfulBundle\Entity\Trade $purchase)
    {
        $this->purchases->removeElement($purchase);
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Add sale
     *
     * @param \PaxfulBundle\Entity\Trade $sale
     *
     * @return User
     */
    public function addSale(\PaxfulBundle\Entity\Trade $sale)
    {
        $this->sales[] = $sale;

        return $this;
    }

    /**
     * Remove sale
     *
     * @param \PaxfulBundle\Entity\Trade $sale
     */
    public function removeSale(\PaxfulBundle\Entity\Trade $sale)
    {
        $this->sales->removeElement($sale);
    }

    /**
     * Get sales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSales()
    {
        return $this->sales;
    }
}
