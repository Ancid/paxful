<?php

namespace PaxfulBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * Trade
 *
 * @ORM\Table(name="trades")
 * @ORM\Entity(repositoryClass="PaxfulBundle\Repository\TradeRepository")
 */
class Trade
{
    const STATUS_PENDING = 'pending';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_SUCCESSFUL = 'successful';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=255)
     */
    protected $status;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $fiatAmount;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $btcAmount;

    /**
     * @ManyToOne(targetEntity="PaxfulBundle\Entity\Offer", inversedBy="trades")
     * @JoinColumn(name="offer_id", referencedColumnName="id")
     */
    protected $offer;

    /**
     * @ManyToOne(targetEntity="PaxfulBundle\Entity\User", inversedBy="purchases")
     * @JoinColumn(name="buyer_id", referencedColumnName="id")
     */
    protected $buyer;

    /**
     * @ManyToOne(targetEntity="PaxfulBundle\Entity\User", inversedBy="sales")
     * @JoinColumn(name="seller_id", referencedColumnName="id")
     */
    protected $seller;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;


    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Trade
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set buyer
     *
     * @param string $buyer
     *
     * @return Trade
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * Get buyer
     *
     * @return User
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set seller
     *
     * @param string $seller
     *
     * @return Trade
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get seller
     *
     * @return User
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Set offer
     *
     * @param Offer $offer
     *
     * @return Trade
     */
    public function setOffer(Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set fiatAmount
     *
     * @param float $fiatAmount
     *
     * @return Trade
     */
    public function setFiatAmount($fiatAmount)
    {
        $this->fiatAmount = $fiatAmount;

        return $this;
    }

    /**
     * Get fiatAmount
     *
     * @return float
     */
    public function getFiatAmount()
    {
        return $this->fiatAmount;
    }

    /**
     * Set btcAmount
     *
     * @param float $btcAmount
     *
     * @return Trade
     */
    public function setBtcAmount($btcAmount)
    {
        $this->btcAmount = $btcAmount;

        return $this;
    }

    /**
     * Get btcAmount
     *
     * @return float
     */
    public function getBtcAmount()
    {
        return $this->btcAmount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Trade
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
