<?php
namespace PaxfulBundle\Controller;

use PaxfulBundle\Entity\Trade;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TradeController extends Controller
{


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function tradesAction()
    {
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $criteria->where($criteria->expr()->eq('seller', $this->getUser()));
        $criteria->orWhere($criteria->expr()->eq('buyer', $this->getUser()));
        $trades = $this->getDoctrine()->getRepository('PaxfulBundle:Trade')->matching($criteria);

        return $this->render('@Paxful/Dashboard/trades.html.twig', [
            'trades' => $trades,
        ]);
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function cancelTradeAction($id)
    {
        $trade = $this->getDoctrine()->getRepository('PaxfulBundle:Trade')->find($id);
        $trade->setStatus(Trade::STATUS_CANCELLED);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl('dashboard_trades'));
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function acceptTradeAction($id)
    {
        try {
            $trade = $this->getDoctrine()->getRepository('PaxfulBundle:Trade')->find($id);
            $this->get('trade_service')->proccessTrade($trade);

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }
        $trades = $this->getDoctrine()->getRepository('PaxfulBundle:Trade')->findBy([
            'seller' => $this->getUser()->getId(),
        ]);

        return $this->redirect($this->generateUrl('dashboard_trades'));
    }
}