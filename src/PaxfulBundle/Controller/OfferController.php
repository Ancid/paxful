<?php
namespace PaxfulBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use PaxfulBundle\Entity\Offer;
use PaxfulBundle\Form\OfferType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


class OfferController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function OffersAction(Request $request)
    {
        $searchArray = $this->collectSearchParams($request);
        $offers = $this->getDoctrine()->getRepository('PaxfulBundle:Offer')->matching($searchArray);

        return $this->render('@Paxful/Offers/list.html.twig', [
            'offers' => $offers,
            'paymentMethods' => Offer::getPaymentMethods(),
        ]);
    }


    /**
     * @param Request $request
     * @return Criteria
     */
    private function collectSearchParams(Request $request): Criteria
    {
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $criteria->where($criteria->expr()->eq('active', 1));
        if (!is_null($request->request->get('clear'))) {
            return $criteria;
        }
        if ($request->request->get('paymentMethod')) {
            $criteria->andWhere($criteria->expr()->eq('paymentMethod', $request->request->get('paymentMethod')));
        }
        if ($request->request->get('minAmount')) {
            $criteria->andWhere($criteria->expr()->gt('minAmount', $request->request->get('minAmount')));
        }
        if ($request->request->get('maxAmount')) {
            $criteria->andWhere($criteria->expr()->lt('maxAmount', $request->request->get('maxAmount')));
        }

        return $criteria;

        $result = $entityRepository->matching($criteria);
//        if ($request->request->get('owner')) {
//            $user = $this->getDoctrine()->getRepository('User')->findOneBy([
//                'email' => $request->request->get('owner')
//            ]);
//            if (!is_null($user)) {
//                $searchArray['owner'] = $request->request->get('paymentMethod');
//            }
//        }
        return $searchArray;
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function useroffersAction()
    {
        $offers = $this->getDoctrine()->getRepository('PaxfulBundle:Offer')->findBy([
            'owner' => $this->getUser()->getId(),
        ]);

        return $this->render('@Paxful/Dashboard/offers.html.twig', [
            'offers' => $offers,
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createOfferAction(Request $request)
    {
        $form = $this->createForm(OfferType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Offer $offer */
            $offer = $form->getData();
            $offer->setOwner($this->getUser());
            //Validate
            try {
                $this->get('trade_service')->validateOffer($offer);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($offer);
                $entityManager->flush();

                return $this->redirect($this->generateUrl('dashboard_offers'));
            } catch (\Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->render('@Paxful/Offers/type.html.twig', [
            'form' => $form->createView(),
            'label' => 'Create'
        ]);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editOfferAction($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $offer = $entityManager->getRepository('PaxfulBundle:Offer')->findOneBy([
            'id' => $id,
        ]);
        $form = $this->createForm(OfferType::class);
        $form->setData($offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $url = $this->generateUrl('dashboard_offers');
            return new RedirectResponse($url);
        }
        return $this->render('@Paxful/Offers/type.html.twig', [
            'form' => $form->createView(),
            'label' => 'Save'
        ]);
    }


    /**
     * @param $id
     * @param $status
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeStatusAction($id, $status, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var Offer $offer */
        $offer = $entityManager->getRepository('PaxfulBundle:Offer')->find($id);
        if ($status == Offer::STATUS_ACTIVE) {
            $active = true;
        } else {
            $active = false;
        }
        $offer->setActive($active);
        $entityManager->flush();

        return $this->redirect(
            $request->headers->get('referer')
        );
    }


    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function buyOfferAction($id, Request $request)
    {
        $amount = (float)$request->request->get('amount');
        /** @var Offer $offer */
        $offer = $this->getDoctrine()->getRepository('PaxfulBundle:Offer')->findOneBy([
            'id' => $id,
            'active' => true,
        ]);

        $offerPrice = $this->get('currency_service')->getOfferPrice($offer);

        try {
            if ($offer->getMinAmount() <= ($amount/$offerPrice) && $offer->getMaxAmount() >= ($amount/$offerPrice)) {
                if ($this->get('trade_service')->createTrade(
                    $offer,
                    $this->getUser(),
                    $amount
                )) {
                    return $this->redirect($this->generateUrl('dashboard_trades'));
                }
            } else {
                throw new \LogicException('Incorrect amount.');
            }
        } catch (\Throwable $e) {
            $this->addFlash('error', $e->getMessage());
        }


        return $this->redirect(
            $request->headers->get('referer')
        );
    }
}