<?php

namespace PaxfulBundle\Repository;


use PaxfulBundle\Entity\Balance;
use PaxfulBundle\Entity\User;


/**
 * BalanceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BalanceRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param User $user
     * @param string $currency
     * @return float
     */
    public function getUserBalanceByCurrency(User $user, string $currency): float
    {
        $balance = 0;
        if (in_array($currency, Balance::getCurrencies())) {
            /** @var Balance $balanceObj */
            $balanceObj = $this->findOneBy([
                'user' => $user->getId(),
                'currency' => $currency,
            ]);
            if (!is_null($balanceObj)) {
                $balance = $balanceObj->getAmount();
            }
        }

        return (float)$balance;
    }


    /**
     * @param User $user
     * @param string $currency
     * @param float $amount
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function changeUserBalance(User $user, string $currency, float $amount): bool
    {
        $balanceObj = $this->findOneBy([
            'user' => $user->getId(),
            'currency' => $currency,
        ]);
        if (is_null($balanceObj)) {
            $balanceObj = new Balance();
            $balanceObj
                ->setUser($user)
                ->setCurrency($currency)
            ;
            $this->_em->persist($balanceObj);
        }
        $balance = $balanceObj->getAmount();
        $balanceObj->setAmount(round($balance + $amount, 8));
        $this->_em->flush();

        return true;
    }
}
