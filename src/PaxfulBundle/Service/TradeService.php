<?php

namespace PaxfulBundle\Service;

use Doctrine\ORM\EntityManager;
use PaxfulBundle\Entity\Balance;
use PaxfulBundle\Entity\Offer;
use PaxfulBundle\Entity\Trade;
use PaxfulBundle\Entity\User;


class TradeService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var CurrencyService
     */
    protected $currencyService;

    public function __construct(EntityManager $entityManager, CurrencyService $currencyService)
    {
        $this->entityManager = $entityManager;
        $this->currencyService = $currencyService;
    }


    /**
     * @param Offer $offer
     * @return bool
     */
    public function validateOffer(Offer $offer): bool
    {
        if ($offer->getMinAmount() > $offer->getMaxAmount()) {
            throw new \LogicException('minAmount can\'t be more than maxAmount.');
        }

        $balance = $this->entityManager->getRepository('PaxfulBundle:Balance')
            ->getUserBalanceByCurrency($offer->getOwner(), Balance::CURRENCY_BTC);

        if ($balance < $offer->getMaxAmount()) {
            throw new \LogicException('You have not enough BTC balance.');
        }

        return true;
    }


    /**
     * @param Offer $offer
     * @param User $buyer
     * @param float $amount
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createTrade(Offer $offer, User $buyer, float $amount): bool
    {
        $balance = $this->entityManager->getRepository('PaxfulBundle:Balance')
            ->getUserBalanceByCurrency($buyer, $offer->getCurrency());

        if ($balance < $amount) {
            throw new \LogicException('You have not enough '.$offer->getCurrency().' balance.');
        }

        $offerPrice = $this->currencyService->getOfferPrice($offer);
        $btcAmount = round($amount/$offerPrice, 8);

        $trade = new Trade();
        $trade
            ->setOffer($offer)
            ->setBuyer($buyer)
            ->setSeller($offer->getOwner())
            ->setFiatAmount($amount)
            ->setBtcAmount($btcAmount)
            ->setStatus(Trade::STATUS_PENDING)
        ;

        $this->entityManager->persist($trade);
        $this->entityManager->flush();

        return true;
    }


    /**
     * @param Trade $trade
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function proccessTrade(Trade $trade): bool
    {
        //Change buyer balances
        $balanceRepo = $this->entityManager->getRepository('PaxfulBundle:Balance');
        $balanceRepo->changeUserBalance(
            $trade->getBuyer(),
            $trade->getOffer()->getCurrency(),
            (-$trade->getFiatAmount())
        );
        $balanceRepo->changeUserBalance(
            $trade->getBuyer(),
            Balance::CURRENCY_BTC,
            (+$trade->getBtcAmount())
        );

        //Change seller balances
        $balanceRepo->changeUserBalance(
            $trade->getSeller(),
            Balance::CURRENCY_BTC,
            (-$trade->getBtcAmount())
        );
        $balanceRepo->changeUserBalance(
            $trade->getSeller(),
            $trade->getOffer()->getCurrency(),
            (+$trade->getFiatAmount())
        );
        //Change status
        $trade->setStatus(Trade::STATUS_SUCCESSFUL);

        //Update offer
        $res = $trade->getBtcAmount() <=> $trade->getOffer()->getMaxAmount();
        switch ($res) {
            case 1:
                $trade->getOffer()->setMaxAmount($trade->getOffer()->getMaxAmount() - $trade->getBtcAmount());
                break;
            case 0:
                $trade->getOffer()->setActive(false);
        }

        $this->entityManager->flush();

        return true;
    }
}