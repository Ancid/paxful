<?php
namespace PaxfulBundle\Service;

use Doctrine\ORM\EntityManager;
use PaxfulBundle\Entity\Balance;
use PaxfulBundle\Entity\Offer;
use PaxfulBundle\Entity\User;

class CurrencyService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param User $user
     * @param float $amount
     * @param string $currency
     */
    public function addMoneyToUser(User $user, float $amount, string $currency): void
    {
        try {
            /** @var Balance $balance */
            $balance = $this->entityManager->getRepository('PaxfulBundle:Balance')->findOneBy([
                'user' => $user->getId(),
                'currency' => $currency
            ]);

            if (is_null($balance)) {
                $balance = new Balance();
                $balance
                    ->setUser($user)
                    ->setCurrency($currency);
                $this->entityManager->persist($balance);
            }

            $balance->setAmount($balance->getAmount() + $amount);
            $this->entityManager->flush();

        } catch (\Doctrine\ORM\OptimisticLockException $e) {
            //log
        }
    }


    /**
     * @param string $currency
     * @return float
     */
    public function getBtcPrice(string $currency = Balance::CURRENCY_USD): float
    {
        //Get currency from somewhere
        return (float)3000;
    }


    /**
     * @param string $currency
     * @return float
     */
    public function getBtcPriceByCurrency(string $currency = Balance::CURRENCY_USD): float
    {
        return round($this->getBtcPrice()/$this->getCurrencies()[$currency], 8);
    }


    /**
     * @param Offer $offer
     * @return float
     */
    public function getOfferPrice(Offer $offer): float
    {
        $margin = 1 + $offer->getMargin();
        $btcPrice = $this->getBtcPriceByCurrency($offer->getCurrency());
        $currencyRate = $this->getCurrencies()[$offer->getCurrency()];
        return round($margin * $btcPrice * $currencyRate, 8);
    }


    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return [
            Balance::CURRENCY_USD => 1.0,
            Balance::CURRENCY_EUR => 1.1326,
            Balance::CURRENCY_NGN => 2.7701,
            Balance::CURRENCY_GPB => 1.33005,
        ];
    }
}