Installing project instructions
========================

```$xslt
php bin/console doctrine:database:create
php bin/console doctrine:schema:update -f
```
After that you can load web page from address you set in parameters (default 127.0.0.1:8000)

